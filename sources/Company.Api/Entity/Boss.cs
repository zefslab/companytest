﻿using System.Collections.Generic;
using Company.Api.Service;

namespace Company.Api.Entity
{
    
    public abstract class Boss : Worker
    {
        public List<Worker> SubWorkers { get; set; }
       
        protected Boss(Company company) : base(company, company.BookkeepingService.PercentByEveryYearCalculationRule)
        {
        }
        /// <summary>
        /// Specific rules for boss
        /// </summary>
        /// <param name="company"></param>
        /// <param name="salaryCalculationRuls"></param>
        protected Boss(Company company, params CalculationRule<Worker>[] salaryCalculationRuls) : base(company, salaryCalculationRuls)
        {
        }
    }
}
