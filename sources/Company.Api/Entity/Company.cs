﻿using System.Collections.Generic;
using Company.Api.Service;

namespace Company.Api.Entity
{
    
    public class Company
    {
        /// <summary>
        /// One rate for all workers type by default in company
        /// </summary>
        public decimal Rate { get; set; }

        public IList<Worker> Workers { get; set; } = new List<Worker>();

        /// <summary>
        /// Бухгалтерия
        /// </summary>
        public IBookkeepingService BookkeepingService { get; }

        public Company(IBookkeepingService bookkeepingService)
        {
            BookkeepingService = bookkeepingService;
        }


    }

    

    

   

    

    
}
