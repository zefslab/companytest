﻿using Company.Api.Service;

namespace Company.Api.Entity
{
    public class Employee : Worker, IHavePercentForYear
    {
        public Employee(Company company): base(company, company.BookkeepingService.PercentByEveryYearCalculationRule)
        {
            
        }
        /// <summary>
        /// Specific rules for worker
        /// </summary>
        /// <param name="company"></param>
        /// <param name="salaryCalculationRuls"></param>
        public  Employee(Company company, params CalculationRule<Worker>[] salaryCalculationRuls) : base(company, salaryCalculationRuls)
        {
           
        }
   

        public double PercentForYear { get; set; } = 3;

        public double PercentForYearLimit { get; set; } = 30;



    }
}
