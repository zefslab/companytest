﻿using Company.Api.Service;

namespace Company.Api.Entity
{
    public class Manager : Boss, IHavePercentForYear, IHavePercentForFirstLineSubordinate
    {
        public Manager(Company company) : base(company)
        {
            AddCalculationRuls(company.BookkeepingService.PercentForFirstLineSubordinateCalculationRule);
        }
        /// <summary>
        /// Specific rules for manager
        /// </summary>
        /// <param name="company"></param>
        /// <param name="salaryCalculationRuls"></param>
        public Manager(Company company, params CalculationRule<Worker>[] salaryCalculationRuls) : base(company, salaryCalculationRuls)
        {
        }

        public double PercentForYear { get; set; } = 5;

        public double PercentForFirstLineSubordinate { get; set; } = 0.5;

        public double PercentForYearLimit { get; set; } = 40;

    }
}
