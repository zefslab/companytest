﻿using Company.Api.Service;

namespace Company.Api.Entity
{
    public class Sales : Boss, IHavePercentForYear, IHavePercentForAllSubordinate
    {
        public Sales(Company company) : base(company)
        {
            AddCalculationRuls(company.BookkeepingService.PercentForAllSubordinateCalculationRule);
        }

        /// <summary>
        /// Specific rules for sales
        /// </summary>
        /// <param name="company"></param>
        /// <param name="salaryCalculationRuls"></param>
        public Sales(Company company, params CalculationRule<Worker>[] salaryCalculationRuls) : base(company, salaryCalculationRuls)
        {
        }

        public double PercentForYear { get; set; } = 1;

        public double PercentForYearLimit { get; set; } = 35;

        public double PercentForAllSubordinate { get; set; } = 0.3;

    }
}
