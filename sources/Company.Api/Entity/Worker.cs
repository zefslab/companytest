﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Company.Api.Service;

namespace Company.Api.Entity
{
    public abstract class Worker
    {
        /// <summary>
        /// Правил исчисления з.п.
        /// Такой список не поддерживает порядок применения правил. В последствии можно применить 
        /// структуру, позволяющую манипулировать очередностью применения правил если этот порядок
        /// будет влиять на конечный результат
        /// </summary>
        private readonly IList<CalculationRule<Worker>> _salaryCalculationRuls;
        
        public string Name { get; set; }
        public DateTime EmploymentDate { get; set; }
        public decimal Salary { get; set; }
        public Boss Boss { get; set; }
        public Company Company { get; set; }

        /// <summary>
        /// Im may by changed in feature worker types
        /// </summary>
        public virtual decimal Rate => Company.Rate;

        public int GetWorkTime()
        {
            return GetWorkTime(DateTime.Now);
        }

        public int GetWorkTime(DateTime dateNow)
        {
            return 5;
        }

        private Worker()
        {
            _salaryCalculationRuls = new List<CalculationRule<Worker>>();
        }

        protected Worker(Company company, params CalculationRule<Worker>[] salaryCalculationRuls) : this()
        {
            Company = company;
            salaryCalculationRuls.ToList().ForEach(_salaryCalculationRuls.Add);
        }

        /// <summary>
        /// Имеется возможность к базовым правилам рассчет з.п. добавить дополнительные правила
        /// </summary>
        /// <param name="rule"></param>
        public void AddCalculationRuls(CalculationRule<Worker> rule)
        {
            _salaryCalculationRuls.Add(rule);
        }
        /// <summary>
        /// Имеется возможность из правил рассчет з.п. удалить правил
        /// </summary>
        /// <param name="rule"></param>
        public void RemoveCalculationRuls(CalculationRule<Worker> rule)
        {
            if (_salaryCalculationRuls.Contains(rule))
            {
                _salaryCalculationRuls.Remove(rule);
            }
        }

        public IList<CalculationRule<Worker>> GetCalculationRules()
        {
            return _salaryCalculationRuls;
        }

    }
}
