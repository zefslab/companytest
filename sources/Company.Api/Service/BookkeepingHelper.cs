﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Company.Api.Entity;

namespace Company.Api.Service
{
    public static class BookkeepingHelper
    {
        /// <summary>
        /// Calkulete count years worked out by worker
        /// </summary>
        /// <param name="worker"></param>
        /// <returns></returns>
        public static int CountWorkedOutYears(Worker worker, DateTime whatNowDate)
        {
            if (whatNowDate > worker.EmploymentDate)
            {
                //TODO  уточниить
                return Convert.ToInt16((whatNowDate - worker.EmploymentDate).TotalDays) / 365;
            }

            return 0;
        }
    }
}
