﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Company.Api.Entity;

namespace Company.Api.Service
{
    /// <summary>
    /// Бухгалтерия
    /// </summary>
    public class BookkeepingService :IBookkeepingService
    {
        private DateTime Now {get {return DateTime.Now;} }
       
        public void CalculateSalaryForAll(Entity.Company company)
        {
            try
            {
                ///Расчет следует проводить рекурсивным методом, начиная с тех, у котрых нет подчиненных
                foreach (var worker in company.Workers)
                {
                    CalculateSalaryForWorker(worker);
                }
            }
            catch (CalculationRuleException ex)
            {
                //Логировать ошибку
            }
           
        }

        public void CalculateSalaryForWorker(Worker worker)
        {
            //Only for boss with subworkes
            if (worker is Boss && ((Boss) worker).SubWorkers.Any())
            {
                var boss = (Boss) worker;
                foreach (var w in boss.SubWorkers)
                {
                    CalculateSalaryForWorker(w);
                }
            }

            worker.Salary = worker.Rate;

           ///Применить все правила начислений к зарплате сотрудника
            foreach (var rule in worker.GetCalculationRules())
            {

                worker.Salary += rule(worker);
            } 
        }

        /// <summary>
        /// Рассчет надбавки к з.п. с учетом количества лет отработанных в организации
        /// </summary>
        /// <typeparam name="TWorker"></typeparam>
        /// <param name="worker"></param>
        /// <returns></returns>
        public decimal PercentByEveryYearCalculationRule<TWorker>(TWorker worker) 
            where TWorker : Worker
        {
            decimal premium = 0;

            var w = worker as IHavePercentForYear;

            if (w != null)
            {
                premium  = Convert.ToDecimal(worker.Rate * Convert.ToDecimal(BookkeepingHelper.CountWorkedOutYears(worker, Now ) * w.PercentForYear/100));

                return premium < (worker.Rate*Convert.ToDecimal(w.PercentForYearLimit/100)) ? premium : worker.Rate* Convert.ToDecimal(w.PercentForYearLimit / 100);
            }

            return premium;
        }
        /// <summary>
        /// Рассчет надбавки к з.п. с учетом числа непосредственных подчиненных 
        /// </summary>
        /// <typeparam name="TBoss"></typeparam>
        /// <param name="worker"></param>
        /// <returns></returns>
        public decimal PercentForFirstLineSubordinateCalculationRule<TBoss>(TBoss worker) 
            where TBoss : Worker
        {
            decimal premium = 0;

            var boss = worker as IHavePercentForFirstLineSubordinate;

            if (boss != null )
                if(worker is Boss)
                {
                    foreach (var subworker in (worker as Boss).SubWorkers)
                    {
                        //TODO  уточниить
                        premium += subworker.Salary*
                                   Convert.ToDecimal(boss.PercentForFirstLineSubordinate/100);
                    }
                }
                else
                {
                    throw new CalculationRuleException("Правило рассчета надбавки за подчиненных" +
                                                       " назначено рядовому сотруднику! ");
                }

            return premium;
        }
        
    }
    
}
