﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Company.Api.Entity;

namespace Company.Api.Service
{
    /// <summary>
    /// Правило рассчета зарплаты. Возвращает скорректированную зарплату.
    /// </summary>
    /// <returns></returns>
    public delegate decimal CalculationRule<in TWorker>(TWorker worker)
        where TWorker : Worker;
}
