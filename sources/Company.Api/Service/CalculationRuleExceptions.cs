﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Api.Service
{
    public class CalculationRuleException : ApplicationException
    {
        public CalculationRuleException(string message) : base(message)
        { }

        public CalculationRuleException(string message, Exception innerException): base(message, innerException)
        { }
    }
}
