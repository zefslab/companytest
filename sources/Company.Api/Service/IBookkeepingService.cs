﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Company.Api.Entity;

namespace Company.Api.Service
{
    /// <summary>
    /// Бухгалтерия, расчетная группа
    /// </summary>
    public interface IBookkeepingService
    {
        /// <summary>
        /// Рассчет з.п. можно  для всех сотрудников в целом
        /// начиная с тех, у которых з.п. не зависит от др. сотрудников
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        void CalculateSalaryForAll(Entity.Company company);

        /// <summary>
        /// При рассчете зарплаты работника может потребоваться рассчитать з.п. всех подчиненных
        /// </summary>
        /// <param name="worker"></param>
        void CalculateSalaryForWorker(Worker worker);

        #region Existing rules
        /// <summary>
        /// Процент за каждый год работы с ограничением по лимиту
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="percent"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        decimal PercentByEveryYearCalculationRule<TWorker>(TWorker worker)
            where TWorker : Worker;

        /// <summary>
        /// Надбавка за непосредственных подчиненных
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="percent"></param>
        /// <returns></returns>
        decimal PercentForFirstLineSubordinateCalculationRule<TBoss>(TBoss worker)
            where TBoss : Worker;
        #endregion
    }


}
