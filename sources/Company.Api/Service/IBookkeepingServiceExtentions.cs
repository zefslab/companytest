﻿using System;
using Company.Api.Entity;

namespace Company.Api.Service
{
    /// <summary>
    /// Методы расширений для бухгалтерского сервиса, добавляющие правила калькуляции надбавки к зарплате
    /// </summary>
    public static class IBookkeepingServiceExtentions
    {
        /// <summary>
        /// Premium for all subordinates. It is sealezed as example for more additionl calculation rules
        /// </summary>
        /// <param name="worker"></param>
        /// <returns>premium</returns>
        public static decimal PercentForAllSubordinateCalculationRule<TBoss>(this IBookkeepingService service,
            TBoss worker)
            where TBoss : Worker
        {
            decimal premium = 0;

            var boss = worker as IHavePercentForAllSubordinate;

            if (boss != null)
            {
                premium = CalculatePremiumForSubordinate(worker, boss.PercentForAllSubordinate);
            }
            else
            {
                throw new CalculationRuleException("Правило рассчета надбавки за подчиненных" +
                                                   " назначено рядовому сотруднику! ");
            }

            return premium;
        }

        /// <summary>
        /// Рекурсивный метод, вычисляющий суммарную надбавку к зарплате начальника за подчиненных всех уровней
        /// </summary>
        /// <param name="worker">подчиненный сотрудник, за которого начисляется надбавка</param>
        /// <param name="percent">процент надбавки за подчиненых</param>
        /// <returns></returns>
        private static decimal CalculatePremiumForSubordinate(Worker worker, double percent)
        {
            decimal premium = 0;

            var boss = worker as Boss;

            if (boss != null)
            {
                foreach (var subworker in boss.SubWorkers)
                {
                    //Ндбавка за всех подчиненных, которые могут быть у непосредсвенного подчиненного
                    premium += CalculatePremiumForSubordinate(subworker, percent);

                    //Надбавка за самого непосредственного подчиненного
                    premium += subworker.Salary * Convert.ToDecimal(percent / 100);
                }
            }

            return premium;

        }
    }

}
