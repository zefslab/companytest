﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Api.Service
{

    public interface IHavePercentForYear
    {
        double PercentForYear { get; set; }
        double PercentForYearLimit { get; set; }
    }

    public interface IHavePercentForFirstLineSubordinate
    {
        double PercentForFirstLineSubordinate { get; set; }
    }
    public interface IHavePercentForAllSubordinate
    {
        double PercentForAllSubordinate { get; set; }
    }

}
