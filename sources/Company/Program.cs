﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.CompilerServices;
using Company.Api.Entity;
using Company.Api.Service;
using Comp = Company.Api.Entity.Company;


namespace Company.Console
{
    class Program
    {
        private static Comp _company;

        private static void Main(string[] args)
        {
            Configure();

            _company.BookkeepingService.CalculateSalaryForWorker(_company.Workers[0]);

        }

        private static void Configure()
        {
            _company = new Comp(new BookkeepingService()) { Rate = 100000M };

            var employee = new Employee(_company) { EmploymentDate = DateTime.Now - TimeSpan.FromDays(365 * 1 + 10) };

            var manager = new Manager(_company);
            var sales = new Sales(_company);

            _company.Workers = new List<Worker>{employee,manager,sales};

            _company.BookkeepingService.CalculateSalaryForWorker(employee);
            
        }



    }
}
