﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using Company.Api;
using Company.Api.Entity;
using Company.Api.Service;
using NUnit.Framework;


namespace UnitTests
{
    [TestFixture]
    public class SalaryCalculationTests
    {
        readonly IBookkeepingService _service = new BookkeepingService();

        #region Simple tests
        [Test]
        public void CalculateSalaryForWorker_EmployeeCalculationOneYear_Ok()
        {
            //Data prepare 
            var company = new Company.Api.Entity.Company(new BookkeepingService()) { Rate = 100000M };
            company.Workers = InitializeWorkers(company);

            //Get employee from up level workers
            var employee = company.Workers.Cast<Employee>().First();
            employee.EmploymentDate = new DateTime(2016, 06, 10); //1 year ago


            //Action
            //See calculation in excel file
            _service.CalculateSalaryForWorker(employee);

            //Asserts
            Assert.AreEqual(employee.Salary, 103000M);

        }
        [Test]
        public void CalculateSalaryForWorker_EmployeeCalculationMore30Percent_Ok()
        {
            //Data prepare 
            var company = new Company.Api.Entity.Company(new BookkeepingService()) { Rate = 100000M };
            company.Workers = InitializeWorkers(company);

            //Get employee from up level workers
            var employee = company.Workers.Cast<Employee>().First();
            employee.EmploymentDate = new DateTime(2006, 06, 10); //11 year ago


            //Action
            //See calculation in excel file
            _service.CalculateSalaryForWorker(employee);

            //Asserts
            Assert.AreEqual(employee.Salary, 100000 * 1.3);

        }

        [Test]
        public void CountWorkedOutYear_Ok()
        {
            //Data prepare 
            var company = new Company.Api.Entity.Company(new BookkeepingService());

            var dataNow = new DateTime(2017, 06, 10);

            //Action
            var oneYear =
                BookkeepingHelper.CountWorkedOutYears(new Employee(company) { EmploymentDate = new DateTime(2016, 06, 10) }, dataNow);
            var zeroYear =
                BookkeepingHelper.CountWorkedOutYears(new Employee(company) { EmploymentDate = new DateTime(2016, 06, 11) }, dataNow);
            var elevenYear =
               BookkeepingHelper.CountWorkedOutYears(new Employee(company) { EmploymentDate = new DateTime(2006, 06, 11) }, dataNow);

            //Assert
            Assert.AreEqual(oneYear, 1);
            Assert.AreEqual(zeroYear, 0);
            Assert.AreEqual(elevenYear, 11);

        }
        #endregion

        #region More difficalt logic tests
        [Test]
        public void CalculateSalaryForWorker_ManagerWithTreeEmployeeCalculation_Ok()
        {
            //Data prepare 
            var company = new Company.Api.Entity.Company(new BookkeepingService()) { Rate = 100000M };
            company.Workers = InitializeWorkers(company);
            //manager with three employees
            var manager = company.Workers.OfType<Manager>().First();
            manager.EmploymentDate = new DateTime(2014, 06, 10); //3 year ago
            manager.SubWorkers.ForEach(w => w.EmploymentDate = new DateTime(2015, 06, 10)); //2 year ago for employees

            //Action
            _service.CalculateSalaryForWorker(manager);

            //Asserts
            //                three employees                   manager
            var salary = (100000 + 100000 * 0.03 * 2) * 3 * 0.005 + 100000 + 100000 * 0.05 * 3;
            Assert.AreEqual(manager.Salary, salary);

        }

        [Test]
        public void CalculateSalaryForWorker_ManagerWithSubmanagerCalculation_Ok()
        {
            //Data prepare 
            var company = new Company.Api.Entity.Company(new BookkeepingService()) { Rate = 100000M };
            company.Workers = InitializeWorkers(company);
            //manager with three employees and one submanager in first layer level
            var manager = company.Workers.OfType<Manager>().Last();
            manager.EmploymentDate = new DateTime(2014, 06, 10); //3 year ago
            manager.SubWorkers.ForEach(w => w.EmploymentDate = new DateTime(2015, 06, 10)); //2 year ago for employees

            var submanager = manager.SubWorkers.OfType<Manager>().First();
            submanager.EmploymentDate = new DateTime(2014, 06, 10); //3 year ago
            submanager.SubWorkers.ForEach(w => w.EmploymentDate = new DateTime(2015, 06, 10)); //2 year ago for employees

            //Action
            _service.CalculateSalaryForWorker(manager);

            //Asserts
            //                          three employees                                   manager
            var subManagerSalary = (100000 + 100000 * 0.03 * 2) * 3 * 0.005 + (100000 + 100000 * 0.05 * 3);
            var managerSalary = subManagerSalary + subManagerSalary * 0.005;

            Assert.AreEqual(manager.Salary, managerSalary);

        }

        [Test]
        public void CalculateSalaryForWorker_SalesCalculation_Ok()
        {
            //Data prepare 
            var company = new Company.Api.Entity.Company(new BookkeepingService()) { Rate = 100000M };
            company.Workers = InitializeWorkers(company);
            //sales with three employees and two submanager
            var sales = company.Workers.OfType<Sales>().First();
            sales.EmploymentDate = new DateTime(2014, 06, 10); //3 year ago
            sales.SubWorkers.OfType<Employee>().ToList().ForEach(w => w.EmploymentDate = new DateTime(2015, 06, 10)); //2 year ago for employees
            var managers = sales.SubWorkers.OfType<Manager>().ToList();
            managers.ForEach(w => w.EmploymentDate = new DateTime(2014, 06, 10)); //3 year ago for employees
            managers.ForEach(x => x.SubWorkers.ForEach(w => w.EmploymentDate = new DateTime(2015, 06, 10))); //2 year ago for employees

            //Action
            _service.CalculateSalaryForWorker(sales);
            var treeYearAgoSalary = sales.Salary;

            sales.EmploymentDate = new DateTime(1977, 06, 10); //40 year ago
            _service.CalculateSalaryForWorker(sales);
            var fortyYearAgoSalary = sales.Salary;
            //Asserts

            var baseSalaryWith3YearPremium = 100000 + 100000*0.01*3; //sales calculaetion foreach year
            var baseSalaryWith40YearPremium = 100000 + 100000 * 0.35; //sales calculaetion with limit criterium

            var subManagerSalary = (100000 + 100000 * 0.03 * 2) * 3 * 0.005
                                    // manager calculation foreach year
                                    + (100000 + 100000 * 0.05 * 3);
            var premiumFomEmployeesAndManagers = 
                              //premium from manager
                              subManagerSalary * 2 * 0.003
                              //premium from 9 employees
                              + (100000 + 100000 * 0.03 * 2) * 9 * 0.003;



            Assert.AreEqual(treeYearAgoSalary, baseSalaryWith3YearPremium + premiumFomEmployeesAndManagers);
            Assert.AreEqual(fortyYearAgoSalary, baseSalaryWith40YearPremium + premiumFomEmployeesAndManagers);

        }

        #endregion
        /// <summary>
        /// Helper
        /// </summary>
        /// <returns></returns>
        private IList<Worker> InitializeWorkers(Company.Api.Entity.Company company)
        {
            var workers = new List<Worker>
            {
                new Employee(company),
                new Manager(company) {SubWorkers = new List<Worker>
                {
                    new Employee(company),
                    new Employee(company),
                    new Employee(company)

                }},
                new Manager(company) {SubWorkers = new List<Worker>
                {
                    new Employee(company),
                    new Employee(company),
                    new Employee(company),
                    new Manager(company) {SubWorkers = new List<Worker>
                    {
                        new Employee(company),
                        new Employee(company),
                        new Employee(company)
                    } }
                }},
                new Sales(company)   {SubWorkers = new List<Worker>
                {
                    new Employee(company),
                    new Employee(company),
                    new Employee(company),
                    new Manager(company) {SubWorkers = new List<Worker>
                    {
                        new Employee(company),
                        new Employee(company),
                        new Employee(company)
                    }},
                    new Manager(company) {SubWorkers = new List<Worker>
                    {
                        new Employee(company),
                        new Employee(company),
                        new Employee(company)
                    }}
                } }
            };

            return workers;
        }


        //[Test]
        public void SelesCalculationOk()
        {
        }
        //[Test]
        public void EmployeeCalculationOk()
        {
        }

    }
}